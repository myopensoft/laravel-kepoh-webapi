<?php

return [
    'webapi' => [
        'enable' => env('KEPOH_WEBAPI_ENABLE', false),
        'webapi_url' => env('KEPOH_WEBAPI_URL'),
        'ssl_verify' => env('KEPOH_WEBAPI_SSL_VERIFY', false),
        'access_token' => env('KEPOH_WEBAPI_ACCESS_TOKEN'),
        'dont_report' => [
            \Illuminate\Auth\AuthenticationException::class,
            \Illuminate\Auth\Access\AuthorizationException::class,
            \Illuminate\Session\TokenMismatchException::class,
            \Illuminate\Validation\ValidationException::class,
        ]
    ]
];
