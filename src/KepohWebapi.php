<?php

namespace Myopensoft\KepohWebapi;

use Illuminate\Support\Arr;
use Myopensoft\KepohWebapi\Jobs\KepohWebapiSendErrorToReceiver;
use Request;

class KepohWebapi
{
    /**
     * Report and log an exception to Telegram.
     * @param \Throwable $exception
     */
    public static function report($exception)
    {
        if (config('kepoh.webapi.enable') && !self::ensureToReportByErrorType($exception)) {
            self::send($exception->getMessage(), $exception->getFile(), $exception->getLine(),
                url()->previous(), Request::method(), Request::url(), json_encode(Request::all()));
        }
    }

    /**
     * Check if the error is needed to be reported
     * @param \Throwable $exception
     * @return bool
     */
    public static function ensureToReportByErrorType($exception): bool
    {
        return !is_null(Arr::first(config('kepoh.webapi.dont_report'),
            function ($type) use ($exception) {
                return $exception instanceof $type;
            }));
    }

    /**
     * Send status to webapi bot
     *
     * @param string $type
     * @param string $message
     * @param string $file
     * @param string $line
     * @param string $url
     * @param string $request_type
     * @param string $param
     * @param string $previous_url
     * @return void
     */
    public static function send(string $message, string $file, string $line, string $url,
                                string $request_type, string $param, string $previous_url)
    {
        $data = self::getData($message, $file, $line, $url, $request_type, $param, $previous_url);

        $job = (new KepohWebapiSendErrorToReceiver($data));

        dispatch($job);
    }

    protected static function getData($message, $file, $line, $url, $request_type, $param, $previous_url)
    {
        return [
            'version' => 'laravel-1.0.0',
            'message' => json_encode(mb_substr($message, 0, 3900)),
            'code_file' => $file,
            'code_line' => $line,
            'url' => $url,
            'request_type' => $request_type,
            'parameter' => $param,
            'previous_url' => $previous_url,
            'user_id' => (auth()->user()->id ?? '-'),
        ];
    }
}
