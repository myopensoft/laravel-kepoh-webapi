<?php

namespace Myopensoft\KepohWebapi\Jobs;

use Http;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class KepohWebapiSendErrorToReceiver implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Http::withOptions([
            'verify' => (boolean)config('kepoh.webapi.ssl_verify')
        ])
            ->post(config('kepoh.webapi.webapi_url'), [
                'access_token' => config('kepoh.webapi.access_token'),
                'data' => $this->data,
            ]);
    }
}
